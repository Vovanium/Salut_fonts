#!/usr/bin/gawk -f
# compare several fonts

$1 == "FONT" {
	fontno += 1
	fontname[fontno] = $2
	fontid[fontno] = FILENAME
	name = ""
	code = ""
}

$1 == "PIXEL_SIZE" {
	pixelsize[fontno] = $2
}

$1 == "STARTCHAR" {
	name = substr($0, 10)
}

$1 == "ENCODING" {
	code = sprintf("%8d", $2 + 0)
	charname[code] = name
}

$1 == "DWIDTH" {
	width[code,fontno] = $2
}

BEGIN {
	PROCINFO["sorted_in"] = "@ind_val_asc"
	threshold = 1.5;
	show_threshold = 1.5;
}

function abs(x) {
	return x<0? -x: x
}

END {
	for(j = 1; j <= fontno; j++) {
		printf("%c. %s\n", j+96, fontid[j]);
	}
	n = asorti(charname, g)
	for(j = 1; j <= n; j += 1) {
		i = g[j]
		ws = hs = ns = 0
		for(k = 1; k <= fontno; k += 1) if(i SUBSEP k in width) {
			ws += width[i,k]
			hs += pixelsize[k]
			ns += 1
		}
		mdx = 0
		aprop = ws/hs
		for(k = 1; k <= fontno; k += 1) if(i SUBSEP k in width) {
			dx = abs(width[i,k] - pixelsize[k]*aprop)
			if(mdx < dx) mdx = dx
		}
		if(1 < ns && show_threshold < mdx) {
			printf("%6X  ", i)
			for(k = 1; k <= fontno; k += 1)
				if(i SUBSEP k in width) {
					dx = width[i,k] - pixelsize[k]*aprop;
					if(threshold < dx) printf("\x1B[41m");
					if(dx < -threshold) printf("\x1B[44m");
					printf("%3d %+5.1f\x1B[0m   ", width[i,k], width[i,k] - pixelsize[k]*aprop)
				} else {
					printf("            ")
				}

			printf("%5.2f  %s\n", aprop, charname[i])
		}
	}
	printf("\n")
}
