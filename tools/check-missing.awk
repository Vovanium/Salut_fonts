#!/usr/bin/gawk -f
function hex(x,   i, y, d) {
	y = 0;
	for(i = 1; i <= length(x); i++) {
		d = index("0123456789ABCDEF", toupper(substr(x, i, 1)));
		if(!(d > 0 && d <= 16)) {
			print "Hexadecimal character required" > "/dev/stderr";
		}
		y = y * 16 + d - 1;
	}
	return y;
}

function missing(x) {
	if(!(x < 32 || x >= 127 && x < 160) && !(x in C)) {
		printf("U+%x %c\n", x, x)
	}
}
FILENAME ~ /\.bdf$/ && $1 == "ENCODING" {
	C[$2] = 1;
}

FILENAME ~ /\.(512|256|set)$/ && match($0, "U\\+([0-9A-Fa-f]+)", A) {
	missing(hex(A[1]))
}