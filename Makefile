#!/usr/bin/make -f

INSTALL_DIR_USER=$(HOME)/.fonts/s/salut

.PHONY : check-metrics-sans check-metrics-sans-x check-metrics-sans-xb check-metrics-sans-xi

check-metrics-sans : check-metrics-sans-x check-metrics-sans-xb check-metrics-sans-xi

check-metrics-sans-x check-metrics-sans-xb check-metrics-sans-xi : check-metrics-sans-x% :
	awk -f tools/check-metrics.awk sans/salut-sans[0-9][0-9]$*.bdf

.PHONY : check-coverage check-coverage-sans check-coverage-mono check-coverage-console

check-coverage : check-coverage-sans check-coverage-mono

check-coverage-sans :
	awk -f tools/check-coverage.awk sans/salut-sans*.bdf

check-coverage-mono :
	awk -f tools/check-coverage.awk mono/salut-mono*.bdf

check-coverage-console :
	awk -f tools/check-coverage.awk mono/salut-mono{08w,11x,14y,16,16b,16i}.bdf

.PHONY: consolefonts

CONSOLE_SET = Uni2 Uni3 CyrSlav Greek
CONSOLE_FONTS = \
	Salut08w Salut08wb \
	Salut11x \
	Salut14y \
	Salut16 Salut16b Salut16i

consolefonts : $(foreach f,$(CONSOLE_FONTS),$(patsubst %,build/consolefonts/%-$f.psf,$(CONSOLE_SET)))

build/consolefonts/Uni2-Salut%.psf : mono/salut-mono%.bdf
	@mkdir -p $(@D)
	bdf2psf $< /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Uni2.512 512 $@

build/consolefonts/Uni3-Salut%.psf : mono/salut-mono%.bdf
	@mkdir -p $(@D)
	bdf2psf $< /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Uni3.512 512 $@

build/consolefonts/CyrSlav-Salut%.psf : mono/salut-mono%.bdf
	@mkdir -p $(@D)
	bdf2psf $< /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/CyrSlav.256 256 $@

build/consolefonts/Greek-Salut%.psf : mono/salut-mono%.bdf
	@mkdir -p $(@D)
	bdf2psf $< /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Greek.256 256 $@

.PHONY : opentype opentype-mono opentype-sans

opentype : opentype-sans opentype-mono

opentype-sans : $(patsubst sans/%.bdf,build/opentype/%.otb,$(wildcard sans/*.bdf))

build/opentype/salut-sans%.otb : sans/salut-sans%.bdf
	mkdir -p $(@D)
	fonttosfnt -o $@ -m 2 -- $^

opentype-mono : $(patsubst mono/%.bdf,build/opentype/%.otb,$(wildcard mono/*.bdf))

build/opentype/salut-mono%.otb : mono/salut-mono%.bdf
	mkdir -p $(@D)
	fonttosfnt -o $@ -- $^

.PHONY : pcf pcf-mono

pcf : pcf-mono

pcf-mono : $(patsubst mono/%.bdf,build/pcf/%.pcf,$(wildcard mono/*.bdf))

build/pcf/salut-mono%.pcf : mono/salut-mono%.bdf
	mkdir -p $(@D)
	bdftopcf -o $@ $^

.PHONY : install-user install-user-mono install-user-sans refresh-cache

FILES_MONO=$(wildcard mono/*.bdf)
INSTALL_USER_FILES_MONO=$(FILES_MONO:mono/%=$(INSTALL_DIR_USER)/%)

install-user-mono : $(INSTALL_USER_FILES_MONO)
	mkfontdir $(INSTALL_DIR_USER)
	fc-cache -fv
	xset fp rehash

$(INSTALL_USER_FILES_MONO) : $(INSTALL_DIR_USER)/% : mono/% $(INSTALL_DIR_USER)
	cp $< $@


FILES_SANS=$(wildcard sans/*.bdf)
INSTALL_USER_FILES_SANS=$(FILES_SANS:sans/%=$(INSTALL_DIR_USER)/%)

install-user-sans : $(INSTALL_USER_FILES_SANS)
	mkfontdir $(INSTALL_DIR_USER)
	fc-cache -fv
	xset fp rehash

$(INSTALL_USER_FILES_SANS) : $(INSTALL_DIR_USER)/% : sans/% $(INSTALL_DIR_USER)
	cp $< $@

install-user : $(INSTALL_USER_FILES_MONO) $(INSTALL_USER_FILES_SANS)
	mkfontdir $(INSTALL_DIR_USER)
	fc-cache -fv
	xset fp rehash

$(INSTALL_DIR_USER):
	mkdir -p $@

